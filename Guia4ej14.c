
/*********************************************************************************************************************
14) (búsqueda binaria) Implemente una función similar de búsqueda, utilizando búsqueda binaria. Un posible prototipo:

???? busqueda_binaria(const int v[], vector donde buscar 
                      size_t n,      largo del vector (dispensable) 
                      int objetivo,  elemento a buscar 
                      size_t izq,    franja del vector donde buscar, 
                      size_t der);   delimitada por izq y der 
*********************************************************************************************************************/
#include "Guia4ej14.h"

int busqueda_binaria (const int v[], size_t n, int objetivo, int mitad, size_t izq, size_t der)
{

	

	
	while (izq<=der)
	{
		mitad=(izq+der)/2;
		if (v[mitad]==objetivo)
		{
			printf("%s %d %s  %d \n",MSJ_NUMERO_PREFIJO, objetivo ,MSJ_OBJETIVO ,mitad+1);
			break;
		}
		if (v[mitad]>objetivo)
		{
			der=mitad;
			mitad=(izq+der)/2;
		}
		if (v[mitad]<objetivo)
		{
			izq=mitad;
			mitad=(izq+der)/2;
		}
		
	}
	return mitad;
	

}

int main (void)
{

int n,objetivo,mitad,izq,der;

	 int v [] = {1,2,3,4,5,6,7,8,9,10};
	izq=0;
	n=10;
	der=n; 



printf("%s\n",MSJ_DATO );

	if (scanf("%d", &objetivo )!=1)
	{
		fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
		return EXIT_FAILURE;
	}


printf("%d\n", busqueda_binaria (v, n, objetivo, mitad, izq, der));


return EXIT_SUCCESS;
}	