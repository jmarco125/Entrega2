
/***************************************************************************************************************
Ejercicio 19 guia 4 
a) Cargue una matriz N×N y calcule su traza (suma de los elementos de la diagonal principal)
b) Cargue una matriz N×M y número y modifique la matriz sumando a cada componente el número recibido
c) Cargue una matriz N×M y número y modifique la matriz mulplicando a cada componente por el número recibido
d) Cargue una matriz N×M y modifique dicha matriz cambiándola por su transpuesta o lea otra matriz B con 
   las dimensiones adecuadas y la cargue con la transpuesta de A,

****************************************************************************************************************/

#include "Guia4ej19.h"

int traza (int M[100][100], size_t filas_columnas)
{
	int suma,i,j;

	for (i=0; i<filas_columnas; i++)
 		
 			for (j=0; j<filas_columnas; j++)
 			{
 				printf("%s M[%d][%d]\n ",MSJ_NUMEROS_MATRIZ, i+1, j+1);

 				if (scanf("%d",&M[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
					if(i==j)
					{
						suma=suma+M[i][j];
					}

					}
 			
 				for (i=0; i<filas_columnas; i++){
 		
 					for (j=0; j<filas_columnas; j++)
 					{
 						printf("%d ",M[i][j]);

 					} 
 					printf("\n");

 				}

					
 				
 	return printf("%s %d\n", MSJ_TRAZA,suma);
}


void modificar_matriz (int M[100][100], size_t filas, size_t columnas, int numero)
{
	int i,j, N[100][100];

	for (i=0; i<filas; i++)
 		
 			for (j=0; j<columnas; j++)
 			{
 				printf("%s M[%d][%d]\n ",MSJ_NUMEROS_MATRIZ, i+1, j+1);

 				if (scanf("%d",&M[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
					
					
						N[i][j]=numero+M[i][j];

					}
 			
 				for (i=0; i<filas; i++){
 		
 					for (j=0; j<columnas; j++)
 					{
 						printf("%d ",N[i][j]);

 					} 
 					printf("\n");
			}
}





void modificar_matriz_multiplicacion (int M[100][100], size_t filas, size_t columnas, int numero1)
{
	int i,j, N[100][100];

	for (i=0; i<filas; i++)
 		
 			for (j=0; j<columnas; j++)
 			{
 				printf("%s M[%d][%d]\n ",MSJ_NUMEROS_MATRIZ, i+1, j+1);

 				if (scanf("%d",&M[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
					
					
						N[i][j]=numero1*M[i][j];

					}
 			
 				for (i=0; i<filas; i++){
 		
 					for (j=0; j<columnas; j++)
 					{
 						printf("%d ",N[i][j]);

 					} 
 					printf("\n");
			}
}




void matriz_transpuesta (int M[100][100], size_t filas, size_t columnas)
{
	int i,j, transpuesta[100][100];

	for (i=0; i<filas; i++)
 		
 			for (j=0; j<columnas; j++)
 			{
 				printf("%s M[%d][%d]\n ",MSJ_NUMEROS_MATRIZ, i+1, j+1);

 				if (scanf("%d",&M[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
				}
 			
 				for (i=0; i<filas; i++)
 		
 					for (j=0; j<columnas; j++)
 					
 						transpuesta[j][i]=M[i][j];

 

				for (i=0; i<filas; i++)
				{
 		
 					for (j=0; j<columnas; j++)
 					{
 						printf("  %d ",transpuesta[i][j]);

 					} 

 					printf("\n\n");
				}
}




void matriz_negativa (int M[100][100], size_t filas, size_t columnas, int numero1)
{
	int i,j, N[100][100],a=0,b=0,c=0,d=0,f=0,g=0,h=0;

	for (i=0; i<filas; i++)
 		
 			for (j=0; j<columnas; j++)
 			{
 				printf("%s M[%d][%d]\n ",MSJ_NUMEROS_MATRIZ, i+1, j+1);

 				if (scanf("%d",&M[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
					if(M[i][j]>0)
					
					a=a+1;

					}

			for (i=0; i<filas; i++){
 		
 					for (j=0; j<columnas; j++)
 					{
 						if (M[i][j]>0 || M[i][j]==0)
 						{
 							b=b+2;

 						}

 					} 
 				}	

 			for (i=0; i<filas; i++){
 		
 					for (j=0; j<columnas; j++)
 					{
 						if (M[i][j]<0)
 						{
 							c=c+3;

 						}

 					} 

				}

			for (i=0; i<filas; i++){
 		
 					for (j=0; j<columnas; j++)
 					{
 						if (M[i][j]<0 || M[i][j]==0)
 						{
 							d=d+4;

 						}

 					} 

				}

			for (i=0; i<filas; i++){
 		
 					for (j=0; j<columnas; j++)
 					{
 						if (M[i][j]>0)
 						{
 							f=f+1;

 						}
 						if (M[i][j]<0)
 						{
 							g=g+1;
 						}
 						if (M[i][j])
 						{
 							h=h+1;
 						}

 					} 

				}

 			for (i=0; i<filas; i++){
 		
 					for (j=0; j<columnas; j++)
 					{
 						printf("%d ",M[i][j]);

 					} 
 					printf("\n");
			}

						if (a==filas*columnas)
							printf("%s\n",MSJ_MATRIZ_POSITIVA);

						if (b==2*(filas*columnas))
							printf("%s\n",MSJ_MATRIZ_NO_NEGATIVA);

						if (c==3*(filas*columnas))
							printf("%s\n",MSJ_MATRIZ_NEGATIVA);

						if (d==4*(filas*columnas))
							printf("%s\n",MSJ_MATRIZ_NO_POSITIVA);

						if (f>0 && g>0 && h>0)
							printf("%s\n",MSJ_MATRIZ_NO_CUMPLE);


}






 int main (void)

 {
		int filas_columnas,i,j, M[100][100], numero, filas, columnas, numero1;

		printf("%s\n",MSJ_FILAS_COLUMNAS );
			if (scanf("%d",&filas_columnas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}
 	
 		traza (M, filas_columnas);
		



		printf("%s\n", MSJ_NUMERO );

			if (scanf("%d",&numero)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		printf("%s\n", MSJ_FILAS );

			if (scanf("%d",&filas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		printf("%s\n", MSJ_COLUMNAS );

			if (scanf("%d",&columnas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		modificar_matriz (M, filas, columnas, numero);






		printf("%s\n", MSJ_NUMERO );

			if (scanf("%d",&numero1)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		printf("%s\n", MSJ_FILAS );

			if (scanf("%d",&filas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		printf("%s\n", MSJ_COLUMNAS );

			if (scanf("%d",&columnas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		modificar_matriz_multiplicacion (M, filas, columnas, numero1);






		printf("%s\n", MSJ_FILAS );

			if (scanf("%d",&filas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		printf("%s\n", MSJ_COLUMNAS );

			if (scanf("%d",&columnas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		matriz_transpuesta (M, filas, columnas);




		printf("%s\n", MSJ_FILAS );

			if (scanf("%d",&filas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		printf("%s\n", MSJ_COLUMNAS );

			if (scanf("%d",&columnas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

 
 		matriz_negativa (M, filas, columnas, numero1);


	return EXIT_SUCCESS;
}

