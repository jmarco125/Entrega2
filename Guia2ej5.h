
#include <stdio.h>
#define MSJ_TEMPERATURA "Ingrse la temperatura del cuerpo celeste:"
#define MSJ_ERR_PREFIJO "ERROR"
#define MSJ_ERR_NO_NUMERICO "valor ingresado no numerico."
#define MSJ_COLOR_AZ "Azul"
#define MSJ_COLOR_BL_AZ "Blanco Azulado"
#define MSJ_COLOR_BL "Blanco"
#define MSJ_COLOR_BL_AM "Blanco Amarillento"
#define MSJ_COLOR_AM "Amarillo"
#define MSJ_COLOR_NJ "Naranja"
#define MSJ_COLOR_RJ "Rojo"

typedef enum {EXIT_SUCCESS, EXIT_FAILURE} error_t;
typedef enum {O, B, A, F, G, K, M} espectral_t;
