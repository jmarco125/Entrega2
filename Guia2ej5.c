/*La clasificación espectral [2] de estrellas (cuerpos celestes) permite agruparlas en función de su temperatura, 
como se muestra en la tabla siguiente

Defina el tipo enumerativo espectral_t con los símbolos correspondientes a las clases espectrales.

Escriba un programa que le pida al usuario la temperatura de un cuerpo celeste y almacene 
la clase del mismo en una variable del tipo espectral_t. Luego, imprima el color de la estrella.
Clase	Temperatura	Color
O	28000 - 50000 K	Azul
B	9600 - 28000 K	Blanco azulado
A	7100 - 9600 K	Blanco
F	5700 - 7100 K	Blanco amarillento
G	4600 - 5700 K	Amarillo
K	3200 - 4600 K	Naranja
M	1700 - 3200 K	Rojo*/

#include "Guia2ej5.h"

int main(void)
{
	int temperatura;

	printf("%s\n",MSJ_TEMPERATURA );
	if (scanf("%d",&temperatura)!=1)
	{
		fprintf(stderr, "%s: %s\n",MSJ_ERR_PREFIJO , MSJ_ERR_NO_NUMERICO);
		return EXIT_FAILURE;
	}

	if (temperatura>=1700&&temperatura<=3200)
		temperatura=M;

		if (temperatura>=3200&&temperatura<=4600)
			temperatura=K;

			if (temperatura>=4600&&temperatura<=5700)
				temperatura=G;

				if (temperatura>=5700&&temperatura<=7100)
					temperatura=F;

					if (temperatura>=7100&&temperatura<=9600)
						temperatura=A;

						if (temperatura>=9600&&temperatura<=28000)
							temperatura=B;

							if (temperatura>=28000&&temperatura<=50000)
								temperatura=O;

	switch (temperatura)
	{
		case O:
		printf("%s\n",MSJ_COLOR_AZ);
		break;

		case B:
		printf("%s\n",MSJ_COLOR_BL_AZ);
		break;

		case A:
		printf("%s\n",MSJ_COLOR_BL);
		break;

		case F:
		printf("%s\n",MSJ_COLOR_BL_AM);
		break;

		case G:
		printf("%s\n",MSJ_COLOR_AM);
		break;

		case K:
		printf("%s\n",MSJ_COLOR_NJ);
		break;

		case M:
		printf("%s\n",MSJ_COLOR_RJ);
		break;
	}

	return EXIT_SUCCESS;
}