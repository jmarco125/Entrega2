
/************************************************************************************************************************
Continuacion ej 19 guia 4

f) sólo para matrices de 2×2 ó 3×3: calcule el determinante.
g) Dadas 2 matrices A∈RN×K y B∈RK×M, calcule el producto almacenando el resultado en una tercera matriz C.
h) Retorne el máximo elemento de la matriz
i) Retorne el máximo de la suma, en valores absolutos, de los elementos de cada columna (norma-1 matricial).

*************************************************************************************************************************/

#include "Guia4ej19.h"

int determinante (int M[20][20], size_t filas_columnas )
{

   int deter;
   deter=(M[0][0]*M[1][1])-(M[1][0]*M[0][1]);

   return printf("%s %d\n",MSJ_DETERMINANTE, deter );

}


void producto_matrices (int A[20][20], int B[20][20])
{

	int C[20][20], i, j;

	for(i=0 ; i<2 ; i++){

		for (j=0; j<3; ++j)
		{
			C[i][j]=(A[i][0]*B[0][j])+(A[i][1]*B[1][j])+(A[i][2]*B[2][j]);
		}
	}

	for (i=0; i<2; i++){
 		
 		for (j=0; j<3; j++)
 			{
 				printf("%d ",C[i][j]);

 			} 
 				printf("\n");
			}
}




int retorna_maximo_elemento (int M[20][20] , size_t filas, size_t columnas )

{
	int max,i,j;

	max=M[0][0];

	for ( i = 0; i < filas; i++)
		for (j = 0; j < columnas; j++)
		{
			if (M[i][j]>max)

				max=M[i][j];

		}

return printf("%s %d\n", MSJ_MAXIMO_VALOR, max);
}


int maximo_de_la_suma (int M[20][20], size_t filas, size_t columnas)
{

	int i,j, suma=0;

	for ( i = 0; i < filas; i++)
		for (j = 0; j < columnas; j++)
		{
			suma= suma + M[i][j];

		}

return printf("%s %d\n", MSJ_MAXIMO_SUMA, suma);
}


int main (void)
{
	int i,j, M[20][20], filas_columnas, A[20][20], B[20][20],filas, columnas;

	/*printf("%s\n",MSJ_FILAS_COLUMNAS1 );
			if (scanf("%d",&filas_columnas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

	for (i=0; i<filas_columnas; i++)
 		
 			for (j=0; j<filas_columnas; j++)
 			{
 				printf("%s M[%d][%d]\n ",MSJ_NUMEROS_MATRIZ, i+1, j+1);

 				if (scanf("%d",&M[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
					

 			}
 			printf("\n");

 			for (i=0; i<filas_columnas; i++){
 		
 					for (j=0; j<filas_columnas; j++)
 					{
 						printf("%d ",M[i][j]);

 					} 
 					printf("\n");
			}

			printf("\n");

		determinante (M, filas_columnas);

			printf("\n");*/




/* producto de 2 matrices*/
		
/*for (i=0; i<2; i++)
 		
 			for (j=0; j<3; j++)
 			{
 				printf("%s %s A[%d][%d]\n ", MSJ_NUMEROS_MATRIZ, MSJ_PREFIJO_MATRIZ_A, i+1, j+1);

 				if (scanf("%d",&A[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
					

 			}
 			printf("\n");

 			for (i=0; i<2; i++){
 		
 		for (j=0; j<3; j++)
 			{
 				printf("%d ",A[i][j]);

 			} 
 				printf("\n");
			}




for (i=0; i<3; i++)
 		
 			for (j=0; j<3; j++)
 			{
 				printf("%s %s B[%d][%d]\n ", MSJ_NUMEROS_MATRIZ, MSJ_PREFIJO_MATRIZ_B, i+1, j+1);

 				if (scanf("%d",&B[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
					

 			}
 			printf("\n");

 	for (i=0; i<3; i++){
 		
 		for (j=0; j<3; j++)
 			{
 				printf("%d ",B[i][j]);

 			} 
 				printf("\n");
			}
				printf("\n");

producto_matrices (A,B);

				printf("\n");
*/



/* retorne el maximo elemento de la matriz*/

		/*printf("%s\n", MSJ_FILAS );

			if (scanf("%d",&filas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		printf("%s\n", MSJ_COLUMNAS );

			if (scanf("%d",&columnas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}



		for (i=0; i<filas; i++)
 		
 			for (j=0; j<columnas; j++)
 			{
 				printf("%s M[%d][%d]\n ", MSJ_NUMEROS_MATRIZ, i+1, j+1);

 				if (scanf("%d",&M[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
					

 			}
 			printf("\n");
 			
 			retorna_maximo_elemento(M,filas,columnas);*/


 /*retorne el máximo de la suma, en valores absolutos, de los elementos de cada columna (norma-1 matricial)*/

 			printf("%s\n", MSJ_FILAS );

			if (scanf("%d",&filas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}

		printf("%s\n", MSJ_COLUMNAS );

			if (scanf("%d",&columnas)!=1) 
				{
					fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
			return EXIT_FAILURE;
			}



		for (i=0; i<filas; i++)
 		
 			for (j=0; j<columnas; j++)
 			{
 				printf("%s M[%d][%d]\n ", MSJ_NUMEROS_MATRIZ, i+1, j+1);

 				if (scanf("%d",&M[i][j])!=1) 
					{
	  				fprintf(stderr, "%s: %s \n", MSJ_ERR_PREFIJO, MSJ_ERR_NO_NUMERICO);
					}
					

 			}
 			printf("\n");

 		maximo_de_la_suma(M,filas,columnas);
 			

	return EXIT_SUCCESS;

}