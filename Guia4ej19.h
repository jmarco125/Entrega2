

#include <stdio.h>
#define MSJ_FILAS_COLUMNAS "Ingrese el numero de filas y columnas: "
#define MSJ_FILAS_COLUMNAS1 "Ingrese el nummero 2 para la matriz de 2 por 2 "
#define MSJ_ERR_PREFIJO "ERROR"
#define MSJ_ERR_NO_NUMERICO "valor ingresado no numerico."
#define MSJ_NUMEROS_MATRIZ "Diguite el nuemero de la matriz"
#define MSJ_TRAZA "La traza de la matriz es: "
#define MSJ_MATRIZ_NEGATIVA "La matriz es negativa. "
#define MSJ_MATRIZ_POSITIVA "La matriz es positiva"
#define MSJ_MATRIZ_NO_NEGATIVA "La matriz es no negativa"
#define MSJ_MATRIZ_NO_POSITIVA "La matriz es no positiva"
#define MSJ_MATRIZ_NO_CUMPLE "No cumple ninguna de las condiciones"
#define MSJ_DETERMINANTE "El determinante de la matriz es: "
#define MSJ_PREFIJO_MATRIZ_A "A [2][3]"
#define MSJ_PREFIJO_MATRIZ_B "B [3][3]"
#define MSJ_MAXIMO_VALOR "El maximo valor de la matriz es: "
#define MSJ_MAXIMO_SUMA "EL maximo de la suma de los elementos de cada columna es: "

#define MSJ_NUMERO "Ingrese un numero: "
#define MSJ_FILAS "Ingrese el numero de filas: "
#define MSJ_COLUMNAS "Ingrese el numero de columnas: "

typedef enum {EXIT_SUCCESS, EXIT_FAILURE} error_t;