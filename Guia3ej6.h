

#include <stdio.h>
#include <math.h>
#define MSJ_RADIO "Ingrese el radio del cilindro:"
#define MSJ_ALTURA "Ingrese la altura del cilindro:"
#define MSJ_ERR_PREFIJO "ERROR"
#define MSJ_ERR_NO_NUMERICO "valor ingresado no numerico."
#define MSJ_DIAMETRO "El diametro del cilindro es"
#define MSJ_CIRCUNF "La circunferencia del cilindro es"
#define MSJ_BASE "La base del cilindro es"
#define MSJ_VOLUMEN "El volumen del cilindro es"
#define M_Pi 3.1416


typedef enum {EXIT_SUCCESS, EXIT_FAILURE} error_t;