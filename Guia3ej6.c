/*Implementar las siguientes funciones. Todas ellas reciben el radio 
y la altura de un cilindro y retornan un número real i.e. r∈R:

cilindro_diametro(),
cilindro_circunferencia(),
cilindro_base(), y
cilindro_volumen().*/


#include "Guia3ej6.h"

double cilindro_diametro(double radio)
{
	double diametro;

	printf("%s\n",MSJ_RADIO );
	if (scanf("%lf",&radio)!=1)
	{
		fprintf(stderr, "%s: %s\n",MSJ_ERR_PREFIJO , MSJ_ERR_NO_NUMERICO);
		return EXIT_FAILURE;
	}

	diametro=2*radio;
	return diametro;
} 


double cilindro_circunferencia (double radio)

{
	double circunferencia;

	printf("%s\n",MSJ_RADIO );
	if (scanf("%lf",&radio)!=1)
	{
		fprintf(stderr, "%s: %s\n",MSJ_ERR_PREFIJO , MSJ_ERR_NO_NUMERICO);
		return EXIT_FAILURE;
	}

	circunferencia=M_Pi*2*radio;

	return circunferencia;
} 


double cilindro_base(double radio)
{
double base;

	printf("%s\n",MSJ_RADIO );
	if (scanf("%lf",&radio)!=1)
	{
		fprintf(stderr, "%s: %s\n",MSJ_ERR_PREFIJO , MSJ_ERR_NO_NUMERICO);
		return EXIT_FAILURE;
	}

	base=M_Pi*radio*radio;
	return base;
} 


double cilindro_volumen(double radio, double altura)
{

	double volumen;

	printf("%s\n",MSJ_RADIO );
	if (scanf("%lf",&radio)!=1)
	{
		fprintf(stderr, "%s: %s\n",MSJ_ERR_PREFIJO , MSJ_ERR_NO_NUMERICO);
		return EXIT_FAILURE;
	}
	printf("%s\n",MSJ_ALTURA );
	if (scanf("%lf",&altura)!=1)
	{
		fprintf(stderr, "%s: %s\n",MSJ_ERR_PREFIJO , MSJ_ERR_NO_NUMERICO);
		return EXIT_FAILURE;
	}

	volumen=M_Pi*radio*radio*altura;
	return volumen;
} 


int main (void)
{
	double radio,altura,diametro,circunferencia,base,volumen;

	
		printf("%s: %.2f\n", MSJ_DIAMETRO, cilindro_diametro(radio));

	
		printf("%s: %.2f\n",MSJ_CIRCUNF, cilindro_circunferencia(radio));

	
		printf("%s: %.2f\n",MSJ_BASE, cilindro_base(radio));

	
		printf("%s %.2f\n",MSJ_VOLUMEN, cilindro_volumen(radio,altura));

	return EXIT_SUCCESS;
}